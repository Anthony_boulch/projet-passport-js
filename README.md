#INTRODUCTION ------

#Ce projet a pour but de réaliser un système de connexion pour un utilisateur.
#Pour réaliser cette application nous avons utilisé NodeJS, Express, PassportJS avec la strategie google ainsi que mongoDB pour stocker les utilisateurs.
#Ici, grâce à la stratégie google, l'utilisateur n'aura pas besoin de s'inscrire via un formulaire classique, 
#mais il aura la possibilité de s'inscrire et de se connecter directement via son compte google.
#Nous avons utilisé cette stratégie car elle permet à l'utilisateur de s'inscrire directement. Aujourd'hui dans les divers application, ce 
#système nous facilite la vie.

---------------------

#EXPLICATION --------

#Une fois que vous avez git clone le projet passport JS

# 1) Ouvrez un terminal à la racine du projet (dossier 'projet-passport-js').

# 2) Tapez la commande : 'npm install'.

# 3) Tapez la commande : 'docker-compose up --build'.

# 4) Vous voyez sur la dernière ligne écrit 'connected to mongodb'.

# 5) Rendez-vous sur votre navigateur à l'adresse 'http://localhost:8080'.

---------------------

#CONNEXION ----------

# 1) Dans la barre de navigation de l'app, cliquez sur 'Login'.

# 2) Sur cette page 'Login', cliquez sur 'Sign in with google'.

# 3) Connectez-vous avec votre compte google.

# 4) Vous serez redirigé vers votre profile.

---------------------

#DECONNEXION

# 1) Il faut que vous soyez connecté.

# 2) Cliquez sur 'Logout'.

# 3) Vous serez redirigé vers la page d'accueil de l'application.